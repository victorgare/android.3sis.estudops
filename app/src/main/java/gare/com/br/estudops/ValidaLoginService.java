package gare.com.br.estudops;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.IntDef;
import android.support.v4.app.NotificationCompat;

public class ValidaLoginService extends Service {
    public ValidaLoginService() {
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        String login = intent.getExtras().getString("login");
        String senha = intent.getExtras().getString("senha");

        NotificationManager nm = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        NotificationCompat.Builder nc = new NotificationCompat.Builder(this);

        if(login.trim().equalsIgnoreCase("ps@fiap.com.br") && senha.trim().equalsIgnoreCase("10")){
            nc.setContentTitle(getString(R.string.lblAvisoSucesso));
            nc.setContentText(getString(R.string.txtCorpoAvisoSucesso));
            nc.setSmallIcon(R.drawable.ic_notify);

        }else{
            nc.setContentTitle(getString(R.string.lblAvisoFalha));
            nc.setContentText(getString(R.string.txtCorpoAvisoFalha));
            nc.setSmallIcon(R.drawable.ic_notify);
        }

        nc.setContentIntent(PendingIntent.getActivity(this, 0, new Intent(this, MainActivity.class), PendingIntent.FLAG_UPDATE_CURRENT));

        nm.notify(100, nc.build());

        return START_STICKY;
    }
}
