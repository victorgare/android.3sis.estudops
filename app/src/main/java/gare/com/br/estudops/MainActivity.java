package gare.com.br.estudops;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {


    private EditText edtLogin;
    private EditText edtSenha;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        edtLogin = (EditText) findViewById(R.id.edtLogin);
        edtSenha = (EditText) findViewById(R.id.edtSenha);
    }

    public void validarLogin(View v){
        String login = edtLogin.getText().toString();
        String senha = edtSenha.getText().toString();

        Intent intent = new Intent(this, ValidaLoginService.class);

        intent.putExtra("login", login);
        intent.putExtra("senha", senha);

        startService(intent);
    }
}
